#
# X Video Explorer makefile
#
VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

CFLAGS = -O

xve: xve.c
	$(CC) -DREVISION=$(VERS) xve.c -lm -o xve

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f xve xve.o splashscreen.h xve.1 *.rpm *.tar.gz
	rm -f MANIFEST *.html

install: xve xve.1
	cp xve /usr/bin/xve
	cp xve.1 /usr/share/man/man1

uninstall:
	rm /usr/bin/xve
	rm /usr/share/man/man1/xve.1

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

SOURCES = README COPYING NEWS control Makefile xve.c xve.adoc

xve-$(VERS).tar.gz: $(SOURCES) xve.1
	@ls $(SOURCES) xve.1 | sed s:^:xve-$(VERS)/: >MANIFEST
	@(cd ..; ln -s xve xve-$(VERS))
	(cd ..; tar -czf xve/xve-$(VERS).tar.gz `cat xve/MANIFEST`)
	@(cd ..; rm xve-$(VERS))

dist: xve-$(VERS).tar.gz

release: xve-$(VERS).tar.gz xve.html
	shipper version=$(VERS) | sh -e -x

refresh: xve.html
	shipper -N -w version=$(VERS) | sh -e -x
